class Code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def [](n)
    @pegs[n]
  end

  def self.random
    choose_four = PEGS.keys.repeated_permutation(4).to_a
    choose_four = choose_four[rand(choose_four.size)]
    Code.new(choose_four)
  end

  def self.parse(string)
    pegs = []
    string.each_char do |a|
      raise_error unless PEGS.keys.include? a.downcase
      pegs << a.downcase
    end
    Code.new(pegs)
  end

  def exact_matches(guess)
    matches = 0
    @pegs.each_index { |i| matches += 1 if @pegs[i] == guess.pegs[i] }
    matches
  end

  def near_matches(guess)
    matches = []
    @pegs.each_index { |i| matches << @pegs[i] if @pegs[i] == guess.pegs[i] }
    @pegs.each do |x|
      if (guess.pegs.include? x) && (matches.count(x) < @pegs.count(x))
        matches << x if matches.count(x) < guess.pegs.count(x)
      end
    end
    matches.size - exact_matches(guess)
  end

  def ==(guess)
    return false unless guess.instance_of? Code
    @pegs == guess.pegs
  end

  PEGS = {
    'r' => :R,
    'g' => :G,
    'b' => :B,
    'y' => :Y,
    'o' => :O,
    'p' => :P
  }

end

class Game
  attr_reader :secret_code, :turn, :over

  def initialize(secret=Code.random)
    secret = Code.parse(secret) unless secret.instance_of? Code
    @secret_code = secret
    @turn = 0
    @over = false
  end

  def get_guess(guess=gets.chomp)
    Code.parse(guess)
  end

  def display_matches(guess)
    guess = Code.parse(guess) unless guess.instance_of? Code
    if @secret_code.exact_matches(guess) == 4
      puts "Congratulations, you've won!"
      @over = true
    else
      puts "exact Matches: #{@secret_code.exact_matches(guess)}."
      puts "near Matches: #{@secret_code.near_matches(guess)}."
    end
  end

  def next_turn
    @turn += 1
    if @turn > 12
      @over = true
      puts "You lost!"
      return
    else
      puts "Turn number #{@turn}. \n Enter your guess (e.g. 'rgby'):"
      guess = get_guess
      display_matches(guess)
    end
  end

end

def play
  puts "Would you like to enter a pattern (e.g. 'rgby')? Leave blank for random."
  pattern = gets.chomp
  game = (pattern.size > 0 ? Game.new(pattern) : Game.new)
  game.next_turn until game.over
end

if __FILE__ == $PROGRAM_NAME
  play
end
